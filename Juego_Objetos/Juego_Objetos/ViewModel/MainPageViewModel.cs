﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;


namespace Juego_Objetos.ViewModel
{
    public class MainPageViewModel : ContentPage, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        CultureInfo cultureInfo = CultureInfo.CreateSpecificCulture("el-GR");
        public ICommand ComandoSi { get; set; }   // comando para procesar las respuestas declaradas y aplciadas en el main
        public ICommand ComandoNo { get; set; }   /// lo mismo
        public ICommand Comando_Colores { get; set; }  // lo mismo

        Random random = new Random();

        int rc = 0;
        int rt = 0;
        public int puntos = 0;   //variable para puntos
        public String color1;   // tipo de color para el stack layout que luego lo voy a cambiar mediante la misma string mas adelante
        public String text_color; // lo mismo pero en texto
        public String botoSi;
        public String botoNo;
        public Boolean contestada = true;  //variable de tipo boolean para mas adelante poder restarle puntos si no contesta en el juego
        public int sincro = 0;  // variable para que como hay mas colores pues nunca daria el mismo color con el mismo texto entonces lo que hago es forzar que cada x tiempo mediante esta variable 
        public int si = 0;  // variable para las respuestas de tipo si
        public int no = 0;  // variable para las respuestas de tipo no
        public Boolean conta_afir = false;
        public int contador_afir = 0;

        ContentPage nav;

        //getter setter para contador de aciertos

        //diferentes getters y setters para mostrar cosas en la interfaz
        // mostrar contador de respuestas correctas en racha
        public int Contador_afir { set { if (contador_afir != value) { contador_afir = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("Contador_afir")); } } } get { return contador_afir; } }
         // mostrar color
        public String Color1 { set { if (color1 != value) { color1 = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("Color1")); } } } get { return color1; } }
        // mostrar el texto de diferente color
        public String Text_color { set { if (text_color != value) { text_color = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("Text_color")); } } } get { return text_color; } }
        public String BotoSi { set { if (botoSi != value) { botoSi = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("BotoSi")); } } } get { return botoSi; } }
        public String BotoNo { set { if (botoNo != value) { botoNo = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("BotoNo")); } } } get { return botoNo; } }
        //mostrar puntos
        public int Puntos { set { if (puntos != value) { puntos = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("Puntos")); } } } get { return puntos; } }
        public int Si { set { if (si != value) { si = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("Si")); } } } get { return si; } }
        public int No { set { if (no != value) { no = value; if (PropertyChanged != null) { PropertyChanged(this, new PropertyChangedEventArgs("No")); } } } get { return no; } }
        public MainPageViewModel(ContentPage navi)
        {
            nav = navi;
            ComandoSi = new Command(Afirmativo);
            ComandoNo = new Command(Negativo);
            BotoSi = "verde1.png";
            BotoNo = "rojo1.png";
            Colores();
        }

        public MainPageViewModel()
        {
        }

        public async void Afirmativo()
        {

            if (rc == rt)   // si 
            {
                Si++;
                Puntos += 1;
                conta_afir = true;
            }
            else
            {
                No++;
                Puntos -= 2;
                conta_afir = false;
            }

            BotoSi = "verde1.png";
            await Task.Delay(200);
            BotoSi = "verde2.png";
            await Task.Delay(250);
            BotoSi = "verde1.png";
        }
        public async void Negativo()
        {

            if (rc == rt)
            {
                No++;
                Puntos -= 2;
                conta_afir = false;
            }
            else
            {
                Si++;
                Puntos += 1;
                conta_afir = true;
                Puntos += 1;
                conta_afir = true;
            }

            BotoNo = "rojo1.png";
            await Task.Delay(200);
            BotoNo = "rojo2.png";
            await Task.Delay(250);
            BotoNo = "rojo1.png";
        }
        async Task Colores()
        {
            while (true)
            {
                contestada = false;
                rc = random.Next(6);  // variable random para cambiar colores
                rt = random.Next(6); // variable random para cambiar texto

                if (rt != rc)   //comprobacion que siempre haya 50% de que coincida el texto con el color
                {
                    sincro++;   // variable para la sincronizacion
                    if (sincro == 2)   //en caso de que sincro sea =2 
                    {
                        rt = rc;      // rt sea igual a rc osea el texto sea igual al color
                        sincro = 0;  //volver a poner la variable de sincronizar en 0
                    }
                }

                switch (rc) //en caso de que random color rc sea igual al 0
                { 

                    case 0:
                        Color1 = "Cyan";  //variable de color que sea cyan
                        break;
                    case 1:
                        Color1 = "Red";
                        break;
                    case 2:
                        Color1 = "Violet";
                        break;
                    case 3:
                        Color1 = "Lime";
                        break;
                    case 4:
                        Color1 = "Orange";
                        break;
                    case 5:
                        Color1 = "White";
                        break;

                }

                switch (rt)
                {
                    case 0:
                        Text_color = "AZUL";  //variable de color que sea cyan
                        break;
                    case 1:
                        Text_color = "ROJO";
                        break;
                    case 2:
                        Text_color = "MORADO";
                        break;
                    case 3:
                        Text_color = "VERDE";
                        break;
                    case 4:
                        Text_color = "NARANJA";
                        break;
                    case 5:
                        Text_color = "BLANCO";
                        break;

                }
                
                await Task.Delay(1500);
                if (conta_afir == true)   //funcion para bonus boolean
                {
                    Contador_afir += 1;
                    if (Contador_afir == 8)  //contador para el bonus si contador_afir llega a 8 que haga lo de abajo
                    {
                        Puntos += 5;   // sumando +5 puntos
                        Contador_afir = 0;   //despues de sumar pongo otra vez el contador_afir 
                    }
                }
                if (conta_afir == false)    // si conta_afir es falso
                {
                    Contador_afir = 0;  //poner contador de contestadas en racha en 0
                }
                if (contestada == false)  // en caso de que contestada sea false
                {
                    Puntos -= 1;   //resta 250puntos
                    conta_afir = false;  // poniendo conta_afir en false
                }

            }  // finsih while

        }

    }
}


