﻿using Juego_Objetos.ViewModel;
using System;
using System.ComponentModel;
using Xamarin.Forms;

namespace Juego_Objetos
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage(object bindingContext)
        {
            InitializeComponent();
            BindingContext = bindingContext;

        }

        private void Inicio(object sender, EventArgs e)
        {
            this.Navigation.PushModalAsync(new Inicio(BindingContext));
        }



    }
}
